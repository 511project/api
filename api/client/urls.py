from django.urls import path

from . import views

urlpatterns = [
    path('sensors_cur', views.SensorsCur.as_view()),
    path('scripts', views.Script.as_view()),
    path('stat', views.Stat.as_view()),
    path('rm_config', views.RmConfig.as_view()),
    path('room_name', views.RoomName.as_view()),
]
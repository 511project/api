from django.shortcuts import render
from django.http import HttpResponse
from rest_framework import viewsets, status
from .serializers import *
from rest_framework.response import Response
from rest_framework.views import APIView
import client.models as mdl
import json

class SensorsCur(APIView):
    """
    API endpoint that allows users to be viewed or edited.
    """
    def get(self, request):
        did = int(request.query_params.get('did'))
        queryset = mdl.SensorsLast.objects.raw(f'''select * from 
                                (select rid, max(id) as id 
                                    from(select sen_l.id, rooms.rid,
                                                sen_l.dt, sen_l.temp_room,
                                                sen_l.temp_valve, sen_l.co2, 
                                                sen_l.humidity, sen_l.people 
                                            from rooms
                                            join sensors_last sen_l
                                                on rooms.rid=sen_l.rid
                                    where rooms.did={did}) main
                                        group by rid) gr
                                    left join sensors_last sns_lst
                                        on gr.id=sns_lst.id''')
        serializer = SensorsCurSerializer(queryset, many=True)
        return Response(serializer.data)


class Script(APIView):
    """
    returns list of device scripts
    """
    def get(self, request):
        try:
            did = int(request.query_params.get('did'))
        except:
             return Response(status=status.HTTP_418_IM_A_TEAPOT,
                                data={"message": f"Invalid param: did"})
        if not Devices.objects.filter(did = did).exists():
            return Response(status=status.HTTP_204_NO_CONTENT,
                                data={"message": f"Device with {did} not exists"})

        queryset = mdl.Script.objects.raw(f'''SELECT s.sc_id, s.name,
                                IF(dc.current_script IS NULL, FALSE, TRUE) as if_current
                                    from script s
                                    left join device_config dc
                                        on s.sc_id=dc.current_script
                                    where s.did={did}''')
        serializer = ScriptSerializer(queryset, many=True)
        return Response(serializer.data)

class Stat(APIView):
    """Return stats"""
    def get(self, request):
        try:
            did = int(request.query_params.get('did'))
        except:
             return Response(status=status.HTTP_418_IM_A_TEAPOT,
                                data={"message": f"Invalid param: did"})
        try:
            rid = int(request.query_params.get('rid'))
        except:
             return Response(status=status.HTTP_418_IM_A_TEAPOT,
                                data={"message": f"Invalid param: rid"})
        try:
            dt = str(request.query_params.get('dt'))
        except:
             return Response(status=status.HTTP_418_IM_A_TEAPOT,
                                data={"message": f"Invalid param: dt"})
        mask = '%%Y-%%m-%%d %%H:%%i:%%S'
        queryset = mdl.SensorsLast.objects.raw(f'''SELECT * from sensors_last where dt between STR_TO_DATE('{dt} 00:00:00', '{mask}')
                        and STR_TO_DATE('{dt} 23:59:59', '{mask}') and rid={rid}''')

        serializer = StatSerializer(queryset, many=True)
        return Response(serializer.data)

class RmConfig(APIView):
    """
    Returns room_id with it's name
    """
    def get(self, request):
        try:
            did = int(request.query_params.get('did'))
        except:
             return Response(status=status.HTTP_418_IM_A_TEAPOT,
                                data={"message": f"Invalid param: did"})
        if not Devices.objects.filter(did = did).exists():
            return Response(status=status.HTTP_204_NO_CONTENT,
                                data={"message": f"Device with {did} not exists"})
        queryset = mdl.Rooms.objects.raw(f'''SELECT * from rooms where did={did}''')
        serializer = RmConfigSerializer(queryset, many=True)
        return Response(serializer.data)



class RoomName(APIView):
    """
    Returns room_id with it's name
        {
        "did":10159,
        "rid":53,
        "room_name":"КККК"
        }
    """
    def put(self, request): 
        body = json.loads(request.body)
        try:
            did = body['did']
        except:
             return Response(status=status.HTTP_418_IM_A_TEAPOT,
                                data={"message": f"Invalid param: did"})
        try:
            rid = body['rid']
        except:
             return Response(status=status.HTTP_418_IM_A_TEAPOT,
                                data={"message": f"Invalid param: rid"})
        try:
            room_name = body['room_name']
        except:
             return Response(status=status.HTTP_418_IM_A_TEAPOT,
                                data={"message": f"Invalid param: room_name"})
        try:
            room = mdl.Rooms.objects.get(rid=rid)
        except mdl.Rooms.DoesNotExist:
            return Response(status=status.HTTP_204_NO_CONTENT,
                                data={"message": f"Not found such rooms"})
        room.name = room_name
        room.save()
        return Response(status=status.HTTP_200_OK,
                                data={"message": f"RM_NAME_CHANGED"})       


        

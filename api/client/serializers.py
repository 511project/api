from rest_framework import serializers
from .models import *

class SensorsCurSerializer(serializers.Serializer):
    rid = serializers.IntegerField(source='rid.rid')
    dt = serializers.DateTimeField()
    temp_room = serializers.IntegerField()
    co2 = serializers.IntegerField()
    humidity = serializers.IntegerField()
    people = serializers.IntegerField()

class ScriptSerializer(serializers.Serializer):
    sc_id = serializers.IntegerField()
    name = serializers.CharField()
    if_current = serializers.BooleanField()

class StatSerializer(serializers.Serializer):
    dt = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S")
    temp_room = serializers.IntegerField()
    temp_valve = serializers.IntegerField()
    co2 = serializers.IntegerField()
    humidity = serializers.IntegerField()
    people = serializers.IntegerField()
    
class RmConfigSerializer(serializers.Serializer):
    rid = serializers.IntegerField()
    name = serializers.CharField()
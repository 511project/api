# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Admin(models.Model):
    uid = models.ForeignKey('User', models.DO_NOTHING, db_column='uid')

    class Meta:
        managed = False
        db_table = 'admin'


class DataLog(models.Model):
    req_id = models.IntegerField(primary_key=True)
    headers = models.TextField(blank=True, null=True)
    params = models.TextField(blank=True, null=True)
    req_json = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'data_log'


class DayGroup(models.Model):
    dg_id = models.AutoField(primary_key=True)
    rg = models.ForeignKey('RoomGroup', models.DO_NOTHING)
    days = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'day_group'


class Developer(models.Model):
    uid = models.ForeignKey('User', models.DO_NOTHING, db_column='uid')

    class Meta:
        managed = False
        db_table = 'developer'


class DeviceConfig(models.Model):
    did = models.ForeignKey('Devices', models.DO_NOTHING, db_column='did')
    current_script = models.IntegerField(blank=True, null=True)
    location = models.CharField(max_length=255, blank=True, null=True)
    timezone = models.CharField(max_length=255, blank=True, null=True)
    back_earlier = models.TimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'device_config'


class DeviceUser(models.Model):
    uid = models.ForeignKey('User', models.DO_NOTHING, db_column='uid')
    did = models.ForeignKey('Devices', models.DO_NOTHING, db_column='did')

    class Meta:
        managed = False
        db_table = 'device_user'


class Devices(models.Model):
    did = models.AutoField(primary_key=True)
    rooms_cnt = models.IntegerField(db_column='rooms')
    name = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return str(self.did) +\
             ' | ' + self.name +\
             f' | rooms cnt:{str(self.rooms_cnt)}'
    class Meta:
        managed = False
        db_table = 'devices'


class DontUse(models.Model):
    st = models.ForeignKey('Settings', models.DO_NOTHING)
    od = models.ForeignKey('OuterDevices', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'dont_use'


class ErrorLog(models.Model):
    req_id = models.IntegerField(primary_key=True)
    traceback = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'error_log'


class MustUse(models.Model):
    st = models.ForeignKey('Settings', models.DO_NOTHING)
    od = models.ForeignKey('OuterDevices', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'must_use'


class OuterDevices(models.Model):
    od_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    did = models.ForeignKey(Devices, models.DO_NOTHING, db_column='did')

    class Meta:
        managed = False
        db_table = 'outer_devices'


class RequestLog(models.Model):
    req_id = models.BigIntegerField(blank=True, null=True)
    status_code = models.TextField(blank=True, null=True)
    req_type = models.TextField(blank=True, null=True)
    req_ip = models.TextField(blank=True, null=True)
    req_route = models.TextField(blank=True, null=True)
    user = models.TextField(blank=True, null=True)
    dt = models.DateTimeField(blank=True, null=True)
    req_date = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'request_log'


class RoomGroup(models.Model):
    rg_id = models.AutoField(primary_key=True)
    sc = models.ForeignKey('Script', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'room_group'


class RoomStatus(models.Model):
    rid = models.ForeignKey('Rooms', models.DO_NOTHING, db_column='rid')
    flow = models.IntegerField(blank=True, null=True)
    ch_temp = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'room_status'


class Rooms(models.Model):
    rid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    did = models.ForeignKey(Devices, models.DO_NOTHING, db_column='did')
    def __str__(self):
        did = str(self.did).split(' | ')[0]
        return  f'{did} | {str(self.rid)} | {self.name}'
    class Meta:
        managed = False
        db_table = 'rooms'


class RoomsInRg(models.Model):
    rg = models.ForeignKey(RoomGroup, models.DO_NOTHING)
    rid = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'rooms_in_rg'


class Script(models.Model):
    sc_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    did = models.ForeignKey(Devices, models.DO_NOTHING, db_column='did')
    change_date = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'script'


class SensorsFull(models.Model):
    rid = models.ForeignKey(Rooms, models.DO_NOTHING, db_column='rid')
    dt = models.DateTimeField()
    temp_room = models.IntegerField(blank=True, null=True)
    temp_valve = models.IntegerField(blank=True, null=True)
    co2 = models.IntegerField(blank=True, null=True)
    humidity = models.IntegerField(blank=True, null=True)
    people = models.IntegerField(blank=True, null=True)

    def __str__(self):
        rid = ' | '.join(str(self.rid).split(' | ')[0:2])
        return  f'''{rid}
                    | {str(self.dt.replace(tzinfo=None))}
                    | temp: {self.temp_room}
                    | hum: {self.humidity}
                    | co2: {self.co2}
                    | people: {self.people}'''
    class Meta:
        managed = False
        db_table = 'sensors_full'


class SensorsLast(models.Model):
    rid = models.ForeignKey(Rooms, models.DO_NOTHING, db_column='rid')
    dt = models.DateTimeField()
    temp_room = models.IntegerField(blank=True, null=True)
    temp_valve = models.IntegerField(blank=True, null=True)
    co2 = models.IntegerField(blank=True, null=True)
    humidity = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    people = models.IntegerField(blank=True, null=True)

    def __str__(self):
        rid = ' | '.join(str(self.rid).split(' | ')[0:2])
        return  f'''{rid}
                    | {str(self.dt.replace(tzinfo=None))}
                    | temp: {self.temp_room}
                    | hum: {self.humidity}
                    | co2: {self.co2}
                    | people: {self.people}'''
    class Meta:
        managed = False
        db_table = 'sensors_last'


class Settings(models.Model):
    dg = models.ForeignKey(DayGroup, models.DO_NOTHING)
    st_id = models.AutoField(primary_key=True)
    st_time = models.DateTimeField()
    temp = models.IntegerField(blank=True, null=True)
    co2 = models.IntegerField(blank=True, null=True)
    humidity = models.IntegerField(blank=True, null=True)
    mute = models.IntegerField(blank=True, null=True)
    at_home = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'settings'


class User(models.Model):
    uid = models.AutoField(primary_key=True)
    email = models.CharField(max_length=255)
    reg_date = models.DateTimeField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return f'''{self.uid}
                    | {self.email}
                    | {self.reg_date.replace(tzinfo=None)}
                    | {self.status}'''
    class Meta:
        managed = False
        db_table = 'user'
